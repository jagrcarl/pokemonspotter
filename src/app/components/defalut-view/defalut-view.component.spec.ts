import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefalutViewComponent } from './defalut-view.component';

describe('DefalutViewComponent', () => {
  let component: DefalutViewComponent;
  let fixture: ComponentFixture<DefalutViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefalutViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefalutViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
