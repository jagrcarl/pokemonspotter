import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';



const auth_config = {
  redirect_uri: 'http://localhost:4200/register', 
  client_id: 'k950GSfgJSsZgqTsD4VcnnkrxF7wz08K5u5dN6RB2dQ',
  scope: 'read_user openid profile email', 
  response_type: 'token',
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.minLength(3)]),
  });


  isLoading: boolean = false;
  loginError: string;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) {
  }

  ngOnInit(): void {
    this.init()
  }

  get password() {
    return this.loginForm.get('password')
  }

  doAuth() {
    window.location.href = "https://gitlab.com/oauth/authorize?" + `${new URLSearchParams(auth_config).toString()}`
    this.init()
  }

  async onRegisterClicked() {
    try {

      const result: any = await (this.auth.user())
      const auth: any = await this.auth.postToDatabase(result.username, result.email, this.password.value)
      this.router.navigateByUrl('/login')
    } 
    catch (e) {
      console.log(e)
    }

  }

  async init() {

    try {
      const result: any = await (this.auth.user())
  
    } catch (e) {
      console.log(e)
    }
  }

}
