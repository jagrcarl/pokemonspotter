import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/player';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-toplist',
  templateUrl: './toplist.component.html',
  styleUrls: ['./toplist.component.scss']
})
export class ToplistComponent implements OnInit {
  name 
  // declare the players object as a User interface 
  players: User[] = []
 
  constructor(private auth: AuthService,private session: SessionService) { }

  ngOnInit(): void {
    this.getplayer()
    this.setName()
  }
  setName() {
    this.name = this.session.get().username
    
  }
  async getplayer() {
    try {

      // get the ranking list from database 
      const result: any = await this.auth.getTop10RankPlayer()
     
      // add the name and points to the playes object based on the User interface 
      for(let player in result.data) {
        this.players.push({name: player, points: result.data[player]})
      }
      
     
    } catch (e) {
      console.error(e.error);

    }
  }

}
