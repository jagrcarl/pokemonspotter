import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAllPokemonsComponent } from './list-all-pokemons.component';

describe('ListAllPokemonsComponent', () => {
  let component: ListAllPokemonsComponent;
  let fixture: ComponentFixture<ListAllPokemonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAllPokemonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAllPokemonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
