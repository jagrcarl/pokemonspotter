import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Pokemon } from 'src/app/pokemon';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-list-all-pokemons',
  templateUrl: './list-all-pokemons.component.html',
  styleUrls: ['./list-all-pokemons.component.scss']
})
export class ListAllPokemonsComponent implements OnInit {
  searchText
  pokemonsSpotted: Pokemon[] = []
  pokemonsNotSpotted: Pokemon[] = []
  pokemonUser
  NotSpotted
  isRare: boolean 

  
  constructor(private auth: AuthService, private session: SessionService) { }
  
  ngOnInit(): void {
    this.getNotSpotted(this.session.get().id)
    this.getSpotted(this.session.get().id)

  }
  /*
    * Fetch the pokemons that has not been spotted from the database
    * Compare the pokemeons with the rare list and set the boolean value to true if is exits in the list
    * The pokemon is the put in the pokemons object
  */ 
  async getNotSpotted(id) {
    try {
      const rare: any =  await this.auth.getRarePokemon()
      
      let rares = rare.data.map(item => item.id)
  
      const result: any = await this.auth.getUnSpotted(id)
      for( let i = 0; i < result.data.length; i++) {
        let isRare = false

        for (let j = 0; j < rares.length; j++) {
          if(rares[j] !== undefined && rares[j] === result.data[i].id){
          
            isRare = true
        }
      }
      
        const test: any = await this.auth.getPokemonByName(result.data[i].pokemonName)
        
        this.pokemonsNotSpotted.push({name: result.data[i].pokemonName, types: test.types, notShiny: test.sprites.front_default, shiny: test.sprites.front_shiny, isShiny: false, isRare: isRare, lon: 1, lat: 1 })
      }
     
    } catch (e) {
      console.error(e.error);

    }
    
  }
  // get the spotted pokemons from the database 
  async getSpotted(id) {

    try {
      const rare: any =  await this.auth.getRarePokemon()
      
      let rares = rare.data.map(item => item.id)
      
      const result: any = await this.auth.getSpotted(id)
      for( let i = 0; i < result.data.length; i++) {
        let isRare = false

        for (let j = 0; j < rares.length; j++) {
          if(rares[j] !== undefined && rares[j] === result.data[i].id){
          
          isRare = true
        }
      }
        const test: any = await this.auth.getPokemonByName(result.data[i].name)
        this.pokemonsSpotted.push({name: result.data[i].name, types: test.types, notShiny: test.sprites.front_default, shiny: test.sprites.front_shiny, isShiny: result.data[i].isShiny, isRare: isRare, lon: result.data[i].latitude, lat: result.data[i].longitude })
      }
      

    } catch (e) {
      console.error(e.error);

    }
}

}
