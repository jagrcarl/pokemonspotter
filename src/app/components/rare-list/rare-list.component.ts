import { Component, OnInit } from '@angular/core';
import { RarePokemon } from 'src/app/rarePokemon';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-rare-list',
  templateUrl: './rare-list.component.html',
  styleUrls: ['./rare-list.component.scss']
})
export class RareListComponent implements OnInit {

  rarePokemons : RarePokemon[] = []
  
  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.getRarePokemons()
  }

  // get the rare list from database 
  async getRarePokemons() {

    const result: any = await this.auth.getRares();
    for(let i=0; i<10; i++) { 
    const test: any = await this.auth.getPokemonByName(result.data[i].pokemonName)
    this.rarePokemons.push({name: result.data[i].pokemonName, timesSpotted: result.data[i].timesSpotted, Url: test.sprites.front_default})  
        
    }
  }

}
