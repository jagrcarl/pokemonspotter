import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RareListComponent } from './rare-list.component';

describe('RareListComponent', () => {
  let component: RareListComponent;
  let fixture: ComponentFixture<RareListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RareListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
