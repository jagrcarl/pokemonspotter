import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service'
import { Pokemon } from 'src/app/pokemon';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {
  selectedColor: boolean = false
  selectedGender = "Female"
  Longitud = 56.8790
  Latitud = 14.8059
  searchText
  date = "2015-08-09"
  time = "13:24:00"


  pokemons: Pokemon[] = []

  constructor(private auth: AuthService, private router: Router, private session: SessionService) { }

  ngOnInit(): void {
    this.init()

  }
  // get all the pokemons from the api 
  async init() {

    try {
      const result: any = await this.auth.getAllPokemon()

      for (let i = 0; i < 151; i++) {
        const test: any = await this.auth.getPokemon(result.results[i].url)
        this.pokemons.push({ name: result.results[i].name, types: test.types, notShiny: test.sprites.front_default, shiny: test.sprites.front_shiny, isShiny: false, isRare: false, lon: this.Longitud, lat: this.Longitud })

      }
    } catch (e) {
      console.error(e.error);

    }

  }
  // get the coordinates from map component 
  getCoordinates(coordinates) {
    this.Longitud = coordinates.lng
    this.Latitud = coordinates.lat
  }
  // set the pokemon to json object
  getPokemon(pokemons: Pokemon, selectedColor, selectedGender, Longitud, Latitud, date, time): void {
    let poke = null
    let typeString = ""
    for (let key in pokemons.types) {
      poke = pokemons.types[key];
      typeString += " " + poke.type.name
    }

    let pokemon = {
      name: pokemons.name,
      type: typeString,
      isShiny: selectedColor,
      gender: selectedGender,
      longitude: Longitud,
      latitude: Latitud,
      date: date + " " + time + ":00"
    }
    console.log(pokemon)
    this.router.navigateByUrl("/listAllPokemon")

    this.auth.addSpottedPokemon(pokemon, this.session.get().username)


  }


}
