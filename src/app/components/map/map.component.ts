import { AfterViewInit, Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {
  @Output() LatLon = new EventEmitter();
  // Set the marker icon

  testIcon = '../../assets/marker-icon.png';
  
  iconDefault = L.icon({
  iconUrl: this.testIcon,
  iconAnchor: [50, 40],
 
});
  marker = L.marker()
  private map;
  constructor() { }

  // Add the map 
  ngAfterViewInit(): void {
    this.map = L.map('map', {
      center: [ 39.8282, -98.5795 ],
      zoom: 3
    });
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
     maxZoom: 19,
  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  });

  tiles.addTo(this.map)
  this.map.on('click', (e) => this.addmarker(e.latlng))
  }
  // set the marker and emit lon and lat 
  addmarker(corrdinates) {
    this.map.removeLayer(this.marker)
    this.LatLon.emit(corrdinates)
    const lat = corrdinates.lat
    const lon = corrdinates.lng

    this.marker = L.marker([lat, lon], {icon: this.iconDefault}).addTo(this.map);
  }
  

}
