import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';





@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(1)]),
    password: new FormControl('', [Validators.required, Validators.minLength(1)])
  });

 isLoading: boolean = false;
  loginError: string;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) {
  }
  ngOnInit(): void {

  }

  get usernameLogin() {
    if (this.session.get() !== false) {
      return this.session.get().username + " is online"
        } else {
      return "Not signed in"
    }
  }


  get username() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }

  async onLoginClicked() {

    this.loginError = '';

try {
    const loginUser: any = await (this.auth.loginToSite(this.username.value))
    if (loginUser.data.password == this.password.value) {
      this.router.navigateByUrl('/defaultList');
      this.session.save({
        token: this.generateToken(32),
        username: this.username.value,
        id: loginUser.data.id
      });
    }
  } catch (e) {
    console.error(e.error.error)
  }
this.isLoading = true;
  }

  onLogoffClicked () {
    this.auth.logoffFromSite()
  }

  //A function to generate a token
   generateToken(length){
    var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];  
    for (var i=0; i<length; i++) {
        var j = (Math.random() * (a.length-1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
}
}

