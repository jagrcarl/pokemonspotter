import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';;
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ListAllPokemonsComponent } from './components/list-all-pokemons/list-all-pokemons.component';
import { MapComponent } from './components/map/map.component';
import { RareListComponent } from './components/rare-list/rare-list.component';
import { ToplistComponent } from './components/toplist/toplist.component';
import { DefalutViewComponent } from './components/defalut-view/defalut-view.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    PokemonComponent,
    ListAllPokemonsComponent,
    MapComponent,
    RareListComponent,
    ToplistComponent,
    DefalutViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
