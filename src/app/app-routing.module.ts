
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard'

//Components
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { LoginComponent } from './components/login/login.component';
import { ListAllPokemonsComponent } from './components/list-all-pokemons/list-all-pokemons.component';
import { RegisterComponent } from './components/register/register.component';
import { RareListComponent } from './components/rare-list/rare-list.component';
import { ToplistComponent } from './components/toplist/toplist.component';
import { DefalutViewComponent } from './components/defalut-view/defalut-view.component';


const routes: Routes = [
  {
    path: 'pokemon',
    component: PokemonComponent,
    canActivate: [ AuthGuard ]
  }, 
  {
    path: 'login',
    component: LoginComponent
  }, 
  {
    path: 'register',
    component: RegisterComponent
  }, 
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/defaultList'
  },
  {
    path: 'listAllPokemon',
    component: ListAllPokemonsComponent,
    canActivate: [ AuthGuard ]
  }, 
  {
    path : 'rareList',
    component: RareListComponent
  },
  {
    path: 'toplist',
    component: ToplistComponent
  }, 
  {
    path: 'defaultList',
    component: DefalutViewComponent
  }
    

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }