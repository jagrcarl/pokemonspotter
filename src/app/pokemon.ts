export interface Pokemon {
    name: string;
    types: [];
    notShiny: URL;
    shiny: URL
    isShiny: boolean,
    isRare: boolean,
    lon: number,
    lat: number
  }