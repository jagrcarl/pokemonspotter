import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  getAllPokemon(): Promise<any> {
    return this.http.get(`${environment.apiUrl}`).toPromise()
  }
  getPokemon(url): Promise<any> {
    return this.http.get(url).toPromise()
  }

  getRarePokemon() {
    console.log(`${environment.getAllSpotted}pokemon/rares`)
    return this.http.get(`${environment.getAllSpotted}pokemon/rares`).toPromise()
  }

  getPokemonByName(name): Promise<any> {
    return this.http.get(`${environment.getPokemonByName}${name}`).toPromise()
  }

  getAllUsers(): Promise<any> {
    return this.http.get(`${environment.getAllUserUrl}`).toPromise()
  }

  getUnSpotted(id) {
    return this.http.get(`${environment.getOneUserUrl}${id}/pokemons/unspotted`).toPromise()
  }

  getSpotted(id) {
    return this.http.get(`${environment.getOneUserUrl}${id}/pokemons`).toPromise()
  }
  // get the top ranked players 
  getTop10RankPlayer() {
    console.log(`${environment.getTopRankPlayers}`)
    return this.http.get(`${environment.getTopRankPlayers}`).toPromise()
  }

  getRares() {
    return this.http.get(`${environment.getAllSpotted}/pokemon/rares`).toPromise()
  }

  user(): Promise<any> {
    let fragment_access_token = (new URLSearchParams(location.hash.substring(1)).get("access_token"))
    return this.http.get('https://gitlab.com/api/v4/user', {
      headers: {
        Authorization: `Bearer ${fragment_access_token}`,
      },
    }).toPromise()
      .catch(res => res.json())
      .then(data => {

        return data
      })
  }

  addSpottedPokemon(pokemon, username): Promise<any> {
    return this.http.post('https://pokemon-spotter-db.herokuapp.com/api/user/name/' + username + '/pokemon' ,
      pokemon
    ).toPromise()
  }

  postToDatabase(accountName, email, password): Promise<any> {
    return this.http.post('https://pokemon-spotter-db.herokuapp.com/api/user', {
      accountName: accountName,
      email: email,
      password: password
    }).toPromise()
  }

  loginToSite(accountName): Promise<any> {
    return this.http.get('https://pokemon-spotter-db.herokuapp.com/api/user/name/' + accountName)
      .toPromise()
  }

  logoffFromSite() {
    localStorage.clear()
    this.router.navigateByUrl('/login');
  }
}

