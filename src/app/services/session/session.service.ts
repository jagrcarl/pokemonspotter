import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private auth: AuthService) { }

  save(session: any) {
    localStorage.setItem('sp_session', JSON.stringify(session));
  }

  get(): any {
    const savedSession = localStorage.getItem('sp_session');
    return savedSession ? JSON.parse(savedSession) : false
  }
}
