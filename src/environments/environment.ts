// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// TODO: clean up urls
export const environment = {
  production: false,
  apiUrl: 'https://pokeapi.co/api/v2/pokemon/?limit=151&offset=0',
  getAllUserUrl: 'https://pokemon-spotter-db.herokuapp.com/api/users',
  getOneUserUrl: 'https://pokemon-spotter-db.herokuapp.com/api/user/',
  getAllSpotted: 'https://pokemon-spotter-db.herokuapp.com/api/',
  getPokemonByName: 'https://pokeapi.co/api/v2/pokemon/',
  dbUrl: 'https://pokemon-spotter-db.herokuapp.com/api/',
  getTopRankPlayers: 'https://pokemon-spotter-db.herokuapp.com/api/user/ranks'
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
