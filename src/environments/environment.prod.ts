export const environment = {
  production: true,
  apiUrl: 'https://pokeapi.co/api/v2/pokemon?limit=151&offset=0',
  getAllUserUrl: 'https://pokemon-spotter-db.herokuapp.com/api/users',
  getOneUserUrl: 'https://pokemon-spotter-db.herokuapp.com/api/user',
  getAllSpotted: 'https://pokemon-spotter-db.herokuapp.com/api/',
  getPokemonByName: 'https://pokeapi.co/api/v2/pokemon/',
  getTopRankPlayers: 'https://pokemon-spotter-db.herokuapp.com/api/user/ranks'
};
