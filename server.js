const express = require('express'),
   path = require('path'),
   fs = require('fs'),
   app = express();

   app.use(express.static('./dist/Pokemon-Spotter'));

   app.get('/*', function(req, res) {
    res.sendFile('index.html', {root: 'dist/Pokemon-Spotter/'}
  );

  });

  app.listen(process.env.PORT || 8080);



    console.log('app running on port');
